import java.util.Iterator;
import java.util.Scanner;

public class Mittelwert {

    public static void main(String[] args) {
        System.out.printf("Der Mittelwert der eingegebenen Zahlen ist %.2f\n", mittelwertBerechnen());
    }

    private static double mittelwertBerechnen() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Von wievielen Zahlen wollen Sie den Mittelwert berechnen?");
        double count = scanner.nextInt();
        System.out.printf("Geben Sie %.0f Zahlen ein, um einen Mittelwert zu berechnen.\n", count);
        double summe = 0;
        int i = 0;
        do {
            summe += scanner.nextDouble();
            i++;
        } while(i < count);
        return summe / count;
    }
}

