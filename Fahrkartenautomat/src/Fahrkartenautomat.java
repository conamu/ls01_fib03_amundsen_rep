import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static final DecimalFormat df = new DecimalFormat("0.00");
    public static final Scanner tastatur = new Scanner(System.in);
    public static boolean bezahlt = false;
    public static boolean verlassen = false;
    public static String[] muenzenAusgabeStringArray = new String[3];
    public static int muenzenAnzahl = 0;
    public static String[][] muenzen = new String[2][10];

    public static void main(String[] args)
    {
        while (true) {
            rueckgeldAusgeben(fahrkartenBezahlen(fahrkartenbestellungErfassen()));
            if (bezahlt) {
                fahrscheinAusgabe();
            }
            if (verlassen) {
                break;
            }
        }
        tastatur.close();
    }
    
    private static int fahrkartenAuswahlMenue() {
        System.out.println("Bitte wählen Sie eine Fahrkarte:" +
                "\n1)  Einzelfahrschein Berlin AB" +
                "\n2)  Einzelfahrschein Berlin BC" +
                "\n3)  Einzelfahrschein Berlin ABC" +
                "\n4)  Kurzstrecke" +
                "\n5)  Tageskarte Berlin AB" +
                "\n6)  Tageskarte Berlin BC" +
                "\n7)  Tageskarte Berlin ABC" +
                "\n8)  Kleingruppen-Tageskarte AB" +
                "\n9)  Kleingruppen-Tageskarte BC" +
                "\n10) Kleingruppen-Tageskarte ABC" +
                "\n11) Weiter" +
                "\n12) Verlassen" +
                "\n\n");
        System.out.print("Ihre Wahl: ");
        while (true) {
            if (tastatur.hasNext()) {
                return tastatur.nextInt();
            } else {
                System.out.println("Bitte geben Sie einen gültigen Wert an.\n");
            }
        }
    }

    private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        java.util.Currency eur = java.util.Currency.getInstance("EUR");
        java.text.NumberFormat format = java.text.NumberFormat.getCurrencyInstance(java.util.Locale.GERMANY);
        format.setCurrency(eur);
        double rueckgeld = 0.0;
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            System.out.println("Noch zu zahlen: " + format.format(zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMuenze = tastatur.nextDouble();
            if (eingeworfeneMuenze == 0.05 || eingeworfeneMuenze == 0.10 || eingeworfeneMuenze == 0.20 || eingeworfeneMuenze == 0.50 || eingeworfeneMuenze == 1 || eingeworfeneMuenze == 2 ) {
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            } else {
                System.out.println("Folgende Münzen werden akzeptiert:\n5ct, 10ct, 20ct, 50ct, 1€, 2€");
            }
        }
        bezahlt = true;
        if (eingezahlterGesamtbetrag >= zuZahlenderBetrag) {
            rueckgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        }
        return rueckgeld;
    }

    private static void fahrscheinAusgabe() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }

    private static double fahrkartenbestellungErfassen() {

        double zuZahlenderBetrag = 0.00;
        double[] kartenPreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};

        do {
            int i = fahrkartenAuswahlMenue();
            if (i <= 10) {
                zuZahlenderBetrag += kartenPreise[i-1];
            } else if (i == 11) {
                return zuZahlenderBetrag;
            } else if (i == 12) {
               System.out.println("Exiting!");
               System.exit(0);
            }
        } while(true);
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {
        java.util.Currency eur = java.util.Currency.getInstance("EUR");
        java.text.NumberFormat format = java.text.NumberFormat.getCurrencyInstance(java.util.Locale.GERMANY);
        df.setRoundingMode(RoundingMode.UP);


        if(rueckgabebetrag > 0.0)
        {
            System.out.println("Der Rückgabebetrag in Höhe von " + format.format(rueckgabebetrag));
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                muenzeErfassen("2", "Euro");
                rueckgabebetrag -= 2.0;
                rueckgabebetrag = round(rueckgabebetrag);
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                muenzeErfassen("1", "Euro");
                rueckgabebetrag -= 1.0;
                rueckgabebetrag = round(rueckgabebetrag);
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                muenzeErfassen("50", "Cent");
                rueckgabebetrag -= 0.5;
                rueckgabebetrag = round(rueckgabebetrag);
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                muenzeErfassen("20", "Cent");
                rueckgabebetrag -= 0.2;
                rueckgabebetrag = round(rueckgabebetrag);
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                muenzeErfassen("10", "Cent");
                rueckgabebetrag -= 0.1;
                rueckgabebetrag = round(rueckgabebetrag);
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                muenzeErfassen("5", "Cent");
                rueckgabebetrag -= 0.05;
                rueckgabebetrag = round(rueckgabebetrag);
            }

        }
        grafischDarstellen();
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }

    private static double round(double ammount){
        return (double) Math.round(ammount * 100) / 100;
    }

    private static void warte(long millisekunden) {
        try {
            Thread.sleep(millisekunden);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static void muenzeErfassen(String betrag, String einheit) {
        muenzen[0][muenzenAnzahl] = betrag;
        muenzen[1][muenzenAnzahl] = einheit;
        muenzenAnzahl++;

    }

    private static void grafikenErstellen(){
        int i = 0;
        int positionTrack = 0;
        int linie = 0;
        while (muenzenAnzahl > 0) {
            if (positionTrack >= 3) {
                linie++;
                positionTrack = 0;
            }
            if (muenzenAnzahl >= 3) {
                muenzenAnzahl -= 3;
                muenzenAusgabeStringArray[linie] = String.format("    *****        *****        *****    \n  *       *    *       *    *       *  \n *    %-2s   *  *    %-2s   *  *    %-2s   * \n *   %-4s  *  *   %-4s  *  *   %-4s  *  \n  *       *    *       *    *       *  \n    *****        *****        *****    ", muenzen[0][i], muenzen[0][i+1], muenzen[0][i+2], muenzen[1][i], muenzen[1][i+1], muenzen[1][i+2]);
                i += 3;
                positionTrack += 3;
            } else if (muenzenAnzahl == 2) {
                muenzenAnzahl -= 2;
                muenzenAusgabeStringArray[linie] = String.format("    *****        *****    \n  *       *    *       *  \n *    %-2s   *  *    %-2s   * \n *   %-4s  *  *   %-4s  *  \n  *       *    *       *  \n    *****        *****    ",  muenzen[0][i], muenzen[0][i+1], muenzen[1][i], muenzen[1][i+1]);
                i += 2;
                positionTrack += 2;
            } else if (muenzenAnzahl == 1) {
                muenzenAnzahl -= 1;
                muenzenAusgabeStringArray[linie] = String.format("    *****    \n  *       *  \n *    %-2s   * \n *   %-4s  *  \n  *       *  \n    *****    ", muenzen[0][i], muenzen[1][i]);
                i += 1;
                positionTrack += 1;
            }
        }
    }

    private static void grafischDarstellen() {
        grafikenErstellen();
        for (int i = 0; i < muenzenAusgabeStringArray.length; i++) {
            if (muenzenAusgabeStringArray[i] == null) {
                System.out.println("==");
            } else {
            System.out.println(muenzenAusgabeStringArray[i]);
            }
        }
    }
}
