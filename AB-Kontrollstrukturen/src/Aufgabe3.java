import java.util.Scanner;

public class Aufgabe3 {

    public static void main(String[] args) {

        Boolean untererFüllstandÜberschritten = true;
        Boolean obererFüllstandÜberschritten = false;
        Boolean filterVerschmutzt = true;
        Boolean alarm = false;
        Boolean pumpe = false;

        if(untererFüllstandÜberschritten != null && obererFüllstandÜberschritten != null) {

            if (obererFüllstandÜberschritten && filterVerschmutzt) {
                alarm = true;
            } else if (obererFüllstandÜberschritten && untererFüllstandÜberschritten) {
                alarm = true;
            } else if (obererFüllstandÜberschritten) {
                pumpe = true;
            }
        } else {
            alarm = true;
        }

        if (alarm) {
            System.out.println("ALARM!");
        } else if (pumpe) {
            System.out.println("Pumpe ist an!");
        }
    }
}
