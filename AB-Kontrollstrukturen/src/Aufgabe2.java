public class Aufgabe2 {

    public static void main(String[] args) {
        System.out.println(checksum("Für diesen Text wird eine Prüfsumme berechnet."));
        System.out.println(checksum("Für diesen Text wird auch eine Prüfsumme berechnet."));
        System.out.println(checksum("Auch für diesen Text wird eine Prüfsumme berechnet."));
        System.out.println(checksum("Und für diesen Text."));

    }

    public static int checksum(String s) {
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {
            sum += s.charAt(i);
        }
        return sum;
    }

}
