public class aufgabe2 {

    public static void main(String[] args) {
        System.out.print(String.format("%1d", 0) + "!" + 
        String.format("%4s", "=") + 
        String.format("%1$-20s", "") + "=" +
        String.format("%4d", 1) + "\n");

        System.out.print(String.format("%1d", 1) + "!" + 
        String.format("%4s", "=") + 
        String.format("%1$-20s", " 1") + "=" +
        String.format("%4d", 1) + "\n");

        System.out.print(String.format("%1d", 2) + "!" + 
        String.format("%4s", "=") + 
        String.format("%1$-20s", " 1 * 2") + "=" +
        String.format("%4d", 2) + "\n");

        System.out.print(String.format("%1d", 3) + "!" + 
        String.format("%4s", "=") + 
        String.format("%1$-20s", " 1 * 2 * 3") + "=" +
        String.format("%4d", 6) + "\n");

        System.out.print(String.format("%1d", 4) + "!" + 
        String.format("%4s", "=") + 
        String.format("%1$-20s", " 1 * 2 * 3  * 4") + "=" +
        String.format("%4d", 24) + "\n");

        System.out.print(String.format("%1d", 5) + "!" + 
        String.format("%4s", "=") + 
        String.format("%1$-20s", " 1 * 2 * 3  * 4 * 5") + "=" +
        String.format("%4d", 120) + "\n");
    }
}
