public class aufgabe3 {
    public static void main(String[] args) {
    System.out.print(String.format("%1$-20s", "Fahrenheit") + "|" + String.format("%12s", "Celsius") + "\n");
    System.out.print("-------------------------------------\n");
    System.out.print(String.format("%1$-20s", "-20") + "|" + String.format("%12s", "-28.89") + "\n");
    System.out.print(String.format("%1$-20s", "-10") + "|" + String.format("%12s", "-23.33") + "\n");
    System.out.print(String.format("%1$-20s", "0") + "|" + String.format("%12s", "-17.78") + "\n");
    System.out.print(String.format("%1$-20s", "20") + "|" + String.format("%12s", "-6.67") + "\n");
    System.out.print(String.format("%1$-20s", "30") + "|" + String.format("%12s", "-1.11") + "\n");
    }
}
